#!/bin/bash
# prepares the container for execution

set -e

# inject the PUBLIC_KEY variable, escaping newlines and forward slashes for sed (https://stackoverflow.com/a/42356201)
PUBLIC_KEY_NL_ESCAPED=${PUBLIC_KEY//$'\n'/\\$'\n'}
PUBLIC_KEY_FS_ESCAPED=${PUBLIC_KEY_NL_ESCAPED//$'/'/\\$'/'}
sed -i "s/\"0ca73cf9-1344-4236-9806-a777d7b16f94\"/\`$PUBLIC_KEY_FS_ESCAPED\`/g" /usr/share/nginx/html/eda2a2e6-8f12-4db3-b62b-9899cf98c640/js/*

# inject the SERVER_ADDRESS variable, escaping forward slashes for sed (https://stackoverflow.com/a/42356201)
SERVER_ADDRESS_FS_ESCAPED=${SERVER_ADDRESS//$'/'/\\$'/'}
sed -i "s/b93d79eb-b4dd-4fc2-990e-38e6e851cc6b/$SERVER_ADDRESS_FS_ESCAPED/g" /usr/share/nginx/html/eda2a2e6-8f12-4db3-b62b-9899cf98c640/js/*

# if the environment variable BASE_URL is set, reconfigures the web-app to be served from the path "/$BASE_URL"
if test -z "$BASE_URL"; then
  true
else
  # inject the BASE_URL variable
  sed -i "s/eda2a2e6-8f12-4db3-b62b-9899cf98c640/$BASE_URL/g" /usr/share/nginx/html/eda2a2e6-8f12-4db3-b62b-9899cf98c640/js/* /usr/share/nginx/html/eda2a2e6-8f12-4db3-b62b-9899cf98c640/index.html
  # moving from eda2a2e6-8f12-4db3-b62b-9899cf98c640 to $BASE_URL
  mv /usr/share/nginx/html/eda2a2e6-8f12-4db3-b62b-9899cf98c640 "/usr/share/nginx/html/$BASE_URL"
fi
