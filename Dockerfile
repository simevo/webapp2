ARG FRONTEND_BASE_IMAGE=deps-frontend-base

FROM node:16 AS deps-frontend-base
# Requirements are installed here to ensure they will be cached.
WORKDIR /app
COPY ./package.json ./yarn.lock ./
RUN yarn

FROM ${FRONTEND_BASE_IMAGE} AS frontend-builder
WORKDIR /app
COPY babel.config.js .eslintrc.js vue.config.js ./
COPY public ./public
COPY src ./src
RUN yarn build

FROM nginx AS frontend
COPY prepare.sh /docker-entrypoint.d/40-prepare.sh
COPY pause.sh /docker-entrypoint.d/50-pause.sh
COPY --from=frontend-builder /app/dist /usr/share/nginx/html/eda2a2e6-8f12-4db3-b62b-9899cf98c640
